﻿using System;

namespace GitExerciseVS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercise 1");
            int firstNum, secondNum;
        EnterFirstNum:
            Console.Write("Enter the first number: ");
            if (!Int32.TryParse(Console.ReadLine(), out firstNum))
            {
                Console.WriteLine("Not valid number!");
                goto EnterFirstNum;
            }

        EnterSecondNum:
            Console.Write("Enter the second number: ");
            if (!Int32.TryParse(Console.ReadLine(), out secondNum))
            {
                Console.WriteLine("Not valid number!");
                goto EnterSecondNum;
            }

            Console.WriteLine(firstNum == secondNum ? "The two numbers are equal!" : "The two numbers are not equal!");

            Console.WriteLine("Exercise 2");
            Console.WriteLine("Complete date: {0}\nShort date: {1}\nDisplay date using 24-hour clock format: {2}", DateTime.Now.ToString(), DateTime.Now.ToShortDateString(), Environment.NewLine + DateTime.Now.Date.ToString("g") + Environment.NewLine + DateTime.Now.ToString("MM/dd/yyyy hh:mm"));

            Console.WriteLine("Exercise 3");
            for (var i = 1; i < 11; i++)
            {
                Console.WriteLine(i.ToString());
            }
            Console.ReadKey();
        }
    }
}
